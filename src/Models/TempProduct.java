package Models;

public class TempProduct {
    private int tempId;
    private String tempName;
    private double tempUnitPrice;
    private int tempQty;
    private String tempImpDate;
    private String state;

    public TempProduct(int tempId, String tempName, double tempUnitPrice, int tempQty, String tempImpDate, String state) {
        this.tempId = tempId;
        this.tempName = tempName;
        this.tempUnitPrice = tempUnitPrice;
        this.tempQty = tempQty;
        this.tempImpDate = tempImpDate;
        this.state = state;
    }

    @Override
    public String toString() {
        return "Models.TempProduct{" +
                "tempId=" + tempId +
                ", tempName='" + tempName + '\'' +
                ", tempUnitPrice=" + tempUnitPrice +
                ", tempQty=" + tempQty +
                ", tempImpDate='" + tempImpDate + '\'' +
                ", state='" + state + '\'' +
                '}';
    }

    public int getTempId() {
        return tempId;
    }

    public void setTempId(int tempId) {
        this.tempId = tempId;
    }

    public String getTempName() {
        return tempName;
    }

    public void setTempName(String tempName) {
        this.tempName = tempName;
    }

    public double getTempUnitPrice() {
        return tempUnitPrice;
    }

    public void setTempUnitPrice(double tempUnitPrice) {
        this.tempUnitPrice = tempUnitPrice;
    }

    public int getTempQty() {
        return tempQty;
    }

    public void setTempQty(int tempQty) {
        this.tempQty = tempQty;
    }

    public String getTempImpDate() {
        return tempImpDate;
    }

    public void setTempImpDate(String tempImpDate) {
        this.tempImpDate = tempImpDate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
