package Events;

import DBConnection.ConnectionDB;
import Models.Product;
import Models.TempProduct;
import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.lang.System.in;

public class EventClass {
    static Scanner sc = new Scanner(in);
    static ArrayList<Product> al = new ArrayList<>();
    static ArrayList<TempProduct> tempList = new ArrayList<>();
    static int id;
    public static String input;
    public static String nTable;
    private static int tabnumber;
    private static int maxrecord;
    private static int row;

    public static void loadData(){

        try{
            getRowAndTbName();
            checkMissSave();
            long startTime = System.nanoTime();
            Statement st = ConnectionDB.getConn().createStatement();
            ResultSet rs = st.executeQuery("SELECT *FROM "+nTable);

            ConnectionDB.getConn().close();
            while (rs.next()){
                al.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getDouble(3),
                        rs.getInt(4),
                        rs.getString(5))
                );
            }

            if (al.isEmpty()){
                id = 1;
            }else {
                id = al.size()+1;
            }

            long endTime = System.nanoTime();
            long timeElapsed = endTime - startTime;
            long durationInMs = TimeUnit.MILLISECONDS.convert(timeElapsed, TimeUnit.NANOSECONDS);
            System.out.println("Current time loading : "+ durationInMs);
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void checkMissSave(){
            readFromTempTB();
            if (!tempList.isEmpty()){
                System.out.print("Do you want to save this record? [Y/y] or [N/n] --> ");
                String ch = sc.next();
                if (ch.equalsIgnoreCase("Y")) {
                    save();
                }else {
                    deleteAllDataInTempTB();
                    tempList.clear();
                    print("Save Canceled!");
                }
            }
    }

    public static void display ( int current){
        int i = row * current;
        int n = i - row;
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
        t.addCell("    ID    ", cellStyle);
        t.addCell("     Name     ", cellStyle);
        t.addCell("  Unit Price  ", cellStyle);
        t.addCell("      Qty     ", cellStyle);
        t.addCell(" Imported Date ", cellStyle);
        for (Product product : al) {
            if (al.indexOf(product) >= n && al.indexOf(product) < i) {
                t.addCell(product.getId() + "", cellStyle);
                t.addCell(product.getName() + "", cellStyle);
                t.addCell(product.getUnitPrice() + "", cellStyle);
                t.addCell(product.getQty() + "", cellStyle);
                t.addCell(product.getImpDate() + "", cellStyle);
            } else if (al.indexOf(product) == i) {
                break;
            }
        }
        int page;
        if (al.size() % row == 0) {
            page = al.size() / row;
        } else {
            page = (al.size() / row) + 1;
        }
        System.out.println(t.render());
        CellStyle cellStyle1 = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t1 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
        t1.addCell("page "+ current + " of " + page +"           -->>>>> Stock Management <<<<<--          All Record : "+ al.size(),cellStyle1);
        System.out.println(t1.render());
        maxrecord = page;
        tabnumber = current;
    }
    public static int getTabnumber () {
        return tabnumber;
    }
    public static int getMaxpage () {
        return maxrecord;
    }

    public static void setRow ( int rows){
        row = rows;
    }

    public static void getRowAndTbName() throws SQLException, ClassNotFoundException {
        String qRow = "SELECT *FROM tb_setrow";
        PreparedStatement ps = ConnectionDB.getConn().prepareStatement(qRow);
        ResultSet rs = ps.executeQuery();
        while (rs.next()){
            row = rs.getInt(1);
            nTable = rs.getString(2);
        }
    }

    public static void updateRow(int newRow) throws SQLException, ClassNotFoundException {

        String update = "UPDATE tb_setrow SET row = ? WHERE row = "+row;
        PreparedStatement ps = ConnectionDB.getConn().prepareStatement(update);
        ps.setInt(1, newRow);
        int result = ps.executeUpdate();
        if (result>0){
            print("Row set successfully!");
        }else {
            print("Row setting fail!");
        }
    }

    public static void write() {
        System.out.println("Input ID : " + id);
        System.out.print("Input Name : ");
        String name = sc.next();
        System.out.print("Input Unit price : ");
        if (checkInput()){
            String price = input;
            System.out.print("Input Qty : ");
            if (checkInput()){
                String qty = input;
                writeData(name, Double.parseDouble(price),Integer.parseInt(qty));
            }
        }
    }

    public static void writeData(String name,double price, int qty){
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
        t.addCell("ID = " + id, cellStyle);
        t.addCell("Name = " + name,cellStyle);
        t.addCell("Unit Price = " + price,cellStyle);
        t.addCell("Quantity = "+qty,cellStyle);
        t.addCell("Import Date = "+formatter.format(date),cellStyle);
        System.out.println(t.render());
        System.out.print("Do you want to add this record? [Y/y] or [N/n] --> ");
        String ch = sc.next();
        if (ch.equalsIgnoreCase("Y")) {
            writeToTempTB(new Product(id,name,price,qty,formatter.format(date)),"write");
            tempList.add(new TempProduct(id,name,price,qty,formatter.format(date), "write"));
            al.add(new Product(id,name,price,qty,formatter.format(date)));
            print("successfully added!");
            id++;
        }else {
            print("Canceled!");
        }
    }

    public static void writeToTempTB(Product product, String state){

        try {
            String insert = "INSERT INTO tb_temp(id, name, unit_price, qty, imp_date, other) VALUES(?,?,?,?,?,?)";
            PreparedStatement ps = ConnectionDB.getConn().prepareStatement(insert);
            ps.setInt(1, product.getId());
            ps.setString(2, product.getName());
            ps.setDouble(3, product.getUnitPrice());
            ps.setInt(4, product.getQty());
            ps.setString(5, product.getImpDate());
            ps.setString(6, state);
            ps.executeUpdate();
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void readFromTempTB(){
        try{
            Statement st = ConnectionDB.getConn().createStatement();
            ResultSet rs = st.executeQuery("SELECT *FROM tb_temp");

            while (rs.next()){
                tempList.add(new TempProduct(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getDouble(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6)
                        ));
            }
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void deleteAllDataInTempTB () {
        try {
            String delete = "DELETE FROM tb_temp";
            PreparedStatement ps = ConnectionDB.getConn().prepareStatement(delete);
            ps.executeUpdate();
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void readData(){
        System.out.print("Read by ID : ");
        if (checkInput()){
            int id = Integer.parseInt(input);
              read(id);
        }
    }
    public static void read(int id){

        for (Product product : al){
            if (id == product.getId()){
                CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                t.addCell("ID = " + product.getId(), cellStyle);
                t.addCell("Name = " + product.getName(),cellStyle);
                t.addCell("Unit Price = " + product.getUnitPrice(),cellStyle);
                t.addCell("Quantity = "+product.getQty(),cellStyle);
                t.addCell("Import Date = "+product.getImpDate(),cellStyle);
                System.out.println(t.render());
            }
        }
    }

    public static void updatePro(){
        System.out.print("Input Models.Product ID to Update : ");
        if (checkInput()) {
            int sid = Integer.parseInt(input);

            for (Product product : al) {
                if (sid == product.getId()) {
                    CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                    Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
                    t.addCell("ID = " + product.getId(), cellStyle);
                    t.addCell("Name = " + product.getName(), cellStyle);
                    t.addCell("Unit Price = " + product.getUnitPrice(), cellStyle);
                    t.addCell("Quantity = " + product.getQty(), cellStyle);
                    t.addCell("Import Date = " + product.getImpDate(), cellStyle);
                    System.out.println(t.render());

                    System.out.print("Do you want to update this record? [Y/y] or [N/n] --> ");
                    String op = sc.next();
                    if (op.equalsIgnoreCase("Y")) {
                        Table t1 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
                        t1.addCell("1.All       " + "2.Name       " + "3.Quantity       " + "4.Unit Price        " + "5.Back to menu", cellStyle);
                        System.out.println(t1.render());
                        System.out.print("Which one do you want to update? --> ");
                        String ch = sc.next();
                        switch (ch) {
                            case "1" -> {
                                updateAll(product);
                            }
                            case "2" -> {
                                updateNamePro(product);
                            }
                            case "3" -> {
                                updateQty(product);
                            }
                            case "4" -> {
                                updatePrice(product);
                            }
                            case "5" -> {
                                return;
                            }
                            default -> System.out.println("Input invalid!");
                        }
                        Table t2 = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.SURROUND);
                        t2.addCell("ID = " + product.getId(), cellStyle);
                        t2.addCell("Name = " + product.getName(), cellStyle);
                        t2.addCell("Unit Price = " + product.getUnitPrice(), cellStyle);
                        t2.addCell("Quantity = " + product.getQty(), cellStyle);
                        t2.addCell("Import Date = " + product.getImpDate(), cellStyle);
                        System.out.println(t2.render());
                        writeToTempTB(product,"update");
                        tempList.add(new TempProduct(
                               product.getId(),
                               product.getName(),
                               product.getUnitPrice(),
                               product.getQty(),
                               product.getImpDate(),
                               "update"
                        ));
                        print("Update Successfully!");
                    } else {
                        print("Update canceled!");
                    }
                }
            }
        }
    }
    private static void updatePrice (Product product){
        System.out.print("Input new Unit Price : ");
        if (checkInput()){
            double price = Double.parseDouble(input);
            product.setUnitPrice(price);
        }
    }
    private static void updateQty (Product product){
        System.out.print("Input new Quantity : ");
        if (checkInput()){
            int qty = Integer.parseInt(input);
            product.setQty(qty);
        }
    }
    private static void updateNamePro (Product product){
        System.out.print("Input new Name : ");
        String name = sc.next();
        product.setName(name);
    }
    private static void updateAll (Product product){
        updateNamePro(product);
        updatePrice(product);
        updateQty(product);
    }

    public static void deletePro () {
        System.out.print("Input Models.Product ID to delete : ");
        if (checkInput()){
            int id = Integer.parseInt(input);
            delete(id);
        }
    }
    public static void delete ( int id){
        for (int i = 0; i < al.size(); i++) {
            if (id == al.get(i).getId()) {
                CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
                Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);
                t.addCell("ID = " + al.get(i).getId(), cellStyle);
                t.addCell("Name = " + al.get(i).getName(),cellStyle);
                t.addCell("Unit Price = " + al.get(i).getUnitPrice(),cellStyle);
                t.addCell("Quantity = "+al.get(i).getQty(),cellStyle);
                t.addCell("Import Date = "+al.get(i).getImpDate(),cellStyle);
                System.out.println(t.render());
                System.out.print("Do you want to delete this record? [Y/y] or [N/n] --> ");
                String op = sc.next();
                if (op.equalsIgnoreCase("Y")) {
                    writeToTempTB(al.get(i),"delete");
                    tempList.add(new TempProduct(
                            al.get(i).getId(),
                            al.get(i).getName(),
                            al.get(i).getUnitPrice(),
                            al.get(i).getQty(),
                            al.get(i).getImpDate(),
                            "delete")
                    );
                    al.remove(al.get(i));
                    print(" Delete successfully!");
                } else {
                    print("Delete canceled!");
                }
            }
        }
    }

    public static void searchPro(){

        System.out.print("Input Name Models.Product to Search : ");
        String sName = sc.next();
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.center);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE, ShownBorders.ALL);
        t.addCell("ID", cellStyle);
        t.addCell("NAME", cellStyle);
        t.addCell("Unit Price", cellStyle);
        t.addCell("Quantity", cellStyle);
        t.addCell("Imported Date", cellStyle);
        for (Product product : al){
            if (product.getName() != null && product.getName().toLowerCase().contains(sName.toLowerCase())){
                t.addCell(product.getId() + "", cellStyle);
                t.addCell(product.getName() + "", cellStyle);
                t.addCell(product.getUnitPrice() + "", cellStyle);
                t.addCell(product.getQty() + "", cellStyle);
                t.addCell(product.getImpDate() + "", cellStyle);
            }
        }
        System.out.println(t.render());

    }

    public static void save(){
        try{
            for (TempProduct tp : tempList){
                switch (tp.getState()) {
                    case "write" -> insertDB(tp.getTempId(), tp.getTempName(), tp.getTempUnitPrice(), tp.getTempQty(), tp.getTempImpDate());
                    case "update" -> updateDB(tp.getTempId(), tp.getTempName(), tp.getTempUnitPrice(), tp.getTempQty());
                    case "delete" -> deleteDB(tp.getTempId());
                }
            }

            tempList.clear();
            deleteAllDataInTempTB();
            print("Save successfully!");
        }catch (Exception e){
            print("Save fail!");
            e.printStackTrace();
        }
    }

    public static void insertDB(int id, String name, double price, int qty, String date) throws SQLException, ClassNotFoundException {

        String insert = "INSERT INTO "+nTable+"(id, name, unit_price, qty, imp_date) VALUES(?,?,?,?,?)";
        PreparedStatement ps = ConnectionDB.getConn().prepareStatement(insert);
        ps.setInt(1, id);
        ps.setString(2, name);
        ps.setDouble(3, price);
        ps.setInt(4, qty);
        ps.setString(5, date);
        ps.executeUpdate();
    }

    public static void updateDB(int id, String name, double price, int qty) throws SQLException, ClassNotFoundException {
            String sql = "UPDATE "+nTable+" SET name=?, unit_price=?, qty=?  WHERE id="+id;
            PreparedStatement ps = ConnectionDB.getConn().prepareStatement(sql);
            ps.setString(1, name);
            ps.setDouble(2, price);
            ps.setInt(3, qty);
            ps.executeUpdate();

    }

    public static void deleteDB(int id) throws SQLException, ClassNotFoundException {
        String delete = "DELETE FROM "+nTable+" WHERE id = "+id;
        PreparedStatement ps = ConnectionDB.getConn().prepareStatement(delete);
        ps.executeUpdate();
    }

    public static void backUp() throws SQLException, ClassNotFoundException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        Date date = new Date();
        Statement st = ConnectionDB.getConn().createStatement();
        String sql = "CREATE TABLE "+
                "tb_"+formatter.format(date)+
                " (id int2," +
                " name varchar(255), " +
                " unit_price float(53), " +
                " qty int4, " +
                " imp_date varchar(255))";
        st.executeUpdate(sql);
        insetToBackupTB("tb_"+formatter.format(date));

        print("Back up successfully in table tb_"+formatter.format(date));
    }

    public static void insetToBackupTB(String tbName) throws SQLException, ClassNotFoundException {
        if (al != null){
            for (Product product : al){
                String insert = "INSERT INTO "+tbName+"(id, name, unit_price, qty, imp_date) VALUES(?,?,?,?,?)";
                PreparedStatement ps = ConnectionDB.getConn().prepareStatement(insert);
                ps.setInt(1, product.getId());
                ps.setString(2, product.getName());
                ps.setDouble(3, product.getUnitPrice());
                ps.setInt(4, product.getQty());
                ps.setString(5, product.getImpDate());
                ps.executeUpdate();
            }
        }
    }

    public static void restore(){
        List<String> results = new ArrayList<>();

        try {

            String sql = "SELECT table_name FROM information_schema.tables WHERE table_schema='public' AND table_type='BASE TABLE'";
            Statement st = ConnectionDB.getConn().createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()){
                if (rs.getString(1).equals("tb_setrow") || rs.getString(1).equals("tb_temp")){
                    continue;
                }
                results.add(rs.getString(1));
            }

        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }

        System.out.println("***** Please choose file back up ******");
        int i = 1;
        for (String s : results){
            System.out.println(i+") "+s);
            i++;
        }
        System.out.print("Please choose file now : ");
        if (checkInput()) {
            int ch = Integer.parseInt(input);

            al.clear();
            nTable = results.get(ch-1);
            updateTbName(nTable);
            try{
                long startTime = System.nanoTime();
                Statement st = ConnectionDB.getConn().createStatement();
                ResultSet rs = st.executeQuery("SELECT *FROM "+results.get(ch-1));

                ConnectionDB.getConn().close();
                while (rs.next()){
                    al.add(new Product(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getDouble(3),
                            rs.getInt(4),
                            rs.getString(5))
                    );
                }
                long endTime = System.nanoTime();
                long timeElapsed = endTime - startTime;
                print("Restore Success! Time read : "+TimeUnit.MILLISECONDS.convert(timeElapsed, TimeUnit.NANOSECONDS));

                id = al.size()+1;
                tempList.clear();
                deleteAllDataInTempTB();
            }catch (SQLException | ClassNotFoundException e){
                e.printStackTrace();
            }
        }
    }

    public static void updateTbName(String tbName){
        try {
            String sql = "UPDATE tb_setrow SET table_name=? WHERE row="+row;
            PreparedStatement ps = ConnectionDB.getConn().prepareStatement(sql);
            ps.setString(1, tbName);
            ps.executeUpdate();

        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void exit(){

        if (!tempList.isEmpty()){
            System.out.print("You have missed save a record! Do you want to save it? [Y/y] or [N/n] : ");
            String ch = sc.next();
            if (ch.equalsIgnoreCase("Y")){
                save();
                System.exit(0);
            }else {
                deleteAllDataInTempTB();
                print("Save canceled!");
                System.exit(0);
            }
        }else {
            System.exit(0);
        }
    }

    public static void resetId() {

        System.out.print("You want to reset ID of all product? [Y/y] or [N/n] : ");
        String ch = sc.next();
        if (ch.equalsIgnoreCase("Y")){
            deleteCurrentDB();
            for (int i=0; i<al.size(); i++){
                al.get(i).setId(i+1);
            }
            reSetIDCurrentDB();
            id = al.size()+1;
            print("ID reset successfully!");
        }else {
            print("Reset ID canceled!");
        }
    }

    public static void deleteCurrentDB () {
        try {
            String delete = "DELETE FROM "+nTable;
            PreparedStatement ps = ConnectionDB.getConn().prepareStatement(delete);
            ps.executeUpdate();
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void reSetIDCurrentDB(){
        try {
            for (Product product : al) {
                String insert = "INSERT INTO " + nTable + "(id, name, unit_price, qty, imp_date) VALUES(?,?,?,?,?)";
                PreparedStatement ps = ConnectionDB.getConn().prepareStatement(insert);
                ps.setInt(1, product.getId());
                ps.setString(2, product.getName());
                ps.setDouble(3, product.getUnitPrice());
                ps.setInt(4, product.getQty());
                ps.setString(5, product.getImpDate());
                ps.executeUpdate();
            }
        }catch (SQLException | ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static void print(String s){
        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.left);
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,ShownBorders.SURROUND);

        t.addCell("    -->>>>> "+s+" <<<<<--    ", cellStyle);
        System.out.println(t.render());
    }

    public static boolean checkInput(){
        boolean check;
        input = sc.next();
        Pattern pattern = Pattern.compile("^-?\\d+(\\.\\d+)?$");
        Matcher matcher = pattern.matcher(input);
        check = matcher.matches();
        if (check){
            return true;
        }else {
            System.out.println("Input Invalid!");
            System.out.print("Please Input Only Number : ");
            return checkInput();
        }
    }
}