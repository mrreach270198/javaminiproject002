package Controllers;

import Events.EventClass;
import Views.Header;
import Views.Menu;

import java.io.IOException;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {

        Header.header();
        EventClass.loadData();
        Menu.menu();

    }
}
